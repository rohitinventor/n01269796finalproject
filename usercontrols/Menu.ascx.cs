﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject.usercontrols
{

    public partial class Menu : System.Web.UI.UserControl
    {

        private string basequery = "SELECT pageid , pagetitle as Title, pagestatus as Status,datecreated as \"Date Created\",datemodified as \"Date Modified\"from pages";


        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }


        //protected void Page_Load(object sender, EventArgs e)

        protected override void OnPreRender(EventArgs e)
        {

            page_status_pick.SelectCommand = basequery;

            if (!IsPostBack)
            {
                PagePick_Manual_Bind1(page_status_pick);
            }


        }


        protected void PagePick_Manual_Bind1(SqlDataSource src)
        {


            DataTable mytbl;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            menu_list.InnerHtml = "<ul id=\"mynavbar\">";
            foreach (DataRow row in mytbl.Rows)
            {

                row["Title"] = "<li><a href=\"EditPage.aspx?pageid=" + row["pageid"] + "\">" + row["Title"] + "</a></li>";

                menu_list.InnerHtml += row["Title"].ToString();

            }
            menu_list.InnerHtml += "</ul>";


        }
    }
}
