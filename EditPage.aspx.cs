﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{

    public partial class EditPage : System.Web.UI.Page
    {

        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }





        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                page_name.InnerHtml = "No Page Found.";
                return;
            }


            page_title.Text = pagerow["pagetitle"].ToString();

            page_content.Text = pagerow["pagecontent"].ToString();
            select_status.SelectedValue = pagerow["pagestatus"].ToString();


        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;


            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }


        protected void Edit_Page(object sender, EventArgs e)
        {

            string name = page_title.Text.ToString();
            string content = page_content.Text.ToString();
            string status = select_status.SelectedItem.Text.ToString();
            DateTime now = DateTime.Now;

            string editquery = "Update pages set pagetitle = '" + name + "', pagecontent = '" + content + "',pagestatus = '" + status + "',datemodified = '" + now.ToString("dd-MM-yyyy ',' HH:mm tt") + "' where pageid=" + pageid;
            debug.InnerHtml = editquery;

            edit_page.UpdateCommand = editquery;
            edit_page.Update();


        }

        //deleting pages

        protected void DelPage(object sender, EventArgs e)
        {

            string delquery = "DELETE FROM pages WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();
            Response.Redirect("ManagePages.aspx");


        }


    }
}
