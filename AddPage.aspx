﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="AddPage.aspx.cs" Inherits="Finalproject.AddPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h3>New page</h3>
   <asp:SqlDataSource runat="server" id="insert_page"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource> 
    
  
        <label>Page title:</label>
       
        <asp:TextBox ID="page_title" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a Page Title">
        </asp:RequiredFieldValidator>
          
      <div>     
     <label>Page content:</label>
     
     <asp:TextBox ID="page_content" TextMode="multiline" Columns="50" Rows="5"  runat="server" CssClass="form-control" Height="300"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter a Page Content">
        </asp:RequiredFieldValidator>
   </div>
   
    <div >
<asp:DropDownList  runat="server" id="select_status" Width="150">
            <asp:ListItem value="pub" Text="Published"></asp:ListItem>
            <asp:ListItem value="dr" Text="Draft"></asp:ListItem>
    </asp:DropDownList>
     
</div>
   
  <div>
    <ASP:Button  CssClass="btn btn-success" id="page_add" Text="Add Page" runat="server" OnClick="addPage"/>      
      
     <asp:Button ID="backbtn"  CssClass="btn btn-primary" runat="server" Text="Back To Pages" CausesValidation="false"
PostBackUrl="~/ManagePages.aspx" />
    </div>
    <div runat="server" id="debug" class="querybox"></div>
   
    
    
</asp:Content>
