﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="EditPage.aspx.cs" Inherits="Finalproject.EditPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    
    
    <h3 runat="server" id="page_name">Edit Page</h3>
    
    
        <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    
    
 
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    
    
     <div >
        <label>Page title:</label>
   
        <asp:TextBox ID="page_title" runat="server" CssClass="form-control"></asp:TextBox>
    
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a Page Title">
        </asp:RequiredFieldValidator>
           
    </div>
    
  
     <label>Page content:</label>

     <asp:TextBox ID="page_content" TextMode="multiline" Columns="50" Rows="5" runat="server" CssClass="form-control" Height="300" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter a Page Content">
        </asp:RequiredFieldValidator>

    
    
  <div class="inputrow">
<asp:DropDownList runat="server" id="select_status" Width="200">
            <asp:ListItem value="Published" Text="Published"></asp:ListItem>
            <asp:ListItem value="Draft" Text="Draft"></asp:ListItem>
    </asp:DropDownList>
     
</div> 
  
    
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
        
           </asp:SqlDataSource>
    
    
 <div>
    <ASP:Button CssClass="btn btn-success" Text="Edit Page" runat="server" OnClick="Edit_Page"/>
     <asp:Button runat="server" id="del_page_btn" CssClass = "btn btn-danger"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Plaese Confirm to delete?')) return false;"
        Text="Delete" />
    <asp:Button ID="backbtn" CssClass = "btn btn-primary" runat="server" Text="Back To Pages" CausesValidation="false"
    PostBackUrl="~/ManagePages.aspx" />
   </div>
        
        
    <div runat="server" id="debug" class="querybox"></div>
    

      
    <div id="del_debug" class="querybox" runat="server"></div>
    

    
    
</asp:Content>
