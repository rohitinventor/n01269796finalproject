﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{

    public partial class ManagePages : System.Web.UI.Page
    {

        //base query
        private string basequery = "SELECT pageid , pagetitle as \"Page Title\", pagestatus as Status,datecreated as \"Created On\",datemodified as \"Modified On\"" +
            "from pages where 1=1";




        protected void Page_Load(object sender, EventArgs e)
        {


            string status = select_status.SelectedItem.ToString();
            if (status == "Published")
            {
                basequery += "And pagestatus like '%Published%'";
            }
            if (status == "Draft")
            {
                basequery += "And pagestatus like '%Draft%'";
            }
            page_list_pick.SelectCommand = basequery;

            page_display.DataSource = PagePick_Manual_Bind1(page_list_pick);
            page_display.DataBind();


        }






        protected DataView PagePick_Manual_Bind1(SqlDataSource src)
        {


            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Action";
            DataColumn serialcol = new DataColumn();
            serialcol.ColumnName = "Serial Number";
            int i = 1;
            mytbl.Columns.Add(editcol);
            mytbl.Columns.Add(serialcol);
            foreach (DataRow row in mytbl.Rows)
            {
                row[serialcol] = i + " ";
                row[editcol] = "<a href=\"EditPage.aspx?pageid=" + row["pageid"] + "\"><span class=\"btn btn-success\">Edit</span></a>";
                i++;
                row["Page Title"] = "<a href=\"EditPage.aspx?pageid=" + row["pageid"] + "\">" + row["Page Title"] + "</a>" + "";



            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }


    }
}
