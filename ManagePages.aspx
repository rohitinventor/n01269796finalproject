﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="ManagePages.aspx.cs" Inherits="Finalproject.ManagePages" %>
<asp:Content id="body" ContentPlaceHolderID="MainContent" runat="server">
    
 <h2 class="jumbotron text-center">Manage Pages</h2> 
 
    <div class="d-flex">
      <div class="p-2 mr-auto">
             <asp:Button ID="addPage" runat="server" Text="Add New" CssClass="btn btn-info" 
PostBackUrl="~/AddPage.aspx" />
           
        </div >
    <div class="p-2">
         Sort By:
<asp:DropDownList runat="server" id="select_status" AutoPostBack ="true" >
            <asp:ListItem value="All" Text="All"></asp:ListItem>
            <asp:ListItem value="Published" Text="Published" ></asp:ListItem>
            <asp:ListItem value="Draft" Text="Draft"></asp:ListItem>
            
    </asp:DropDownList> 
    </div>
    </div>    
        
<asp:SqlDataSource runat="server" ID="page_list_pick"
    ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
</asp:SqlDataSource>
    
    
<div>  
<asp:DataGrid id="page_display" runat="server" CssClass="table table-striped" ></asp:DataGrid>
</div>  


</asp:Content>