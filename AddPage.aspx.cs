﻿using System;
using System.Web;
using System.Web.UI;

namespace Finalproject
{

    public partial class AddPage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO pages (pagetitle,pagecontent,pagestatus,datecreated,datemodified) VALUES";



        protected void addPage(object sender, EventArgs e)
        {


            string name = page_title.Text.ToString();
            string content = page_content.Text.ToString();
            string status = select_status.SelectedItem.Text.ToString();
            DateTime now = DateTime.Now;

            addquery += "('" + name + "','" + content + "','" + status + "','" + now.ToString("dd-MM-yyyy ',' HH:mm tt") + "','" + now.ToString("dd-MM-yyyy ',' HH:mm tt") + "')";

            debug.InnerHtml = addquery;

            insert_page.InsertCommand = addquery;
            insert_page.Insert();
        }

    }
}
